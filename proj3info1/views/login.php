<head>
	<link href="estilo.css" rel="stylesheet">
</head>
	<form class="form-signin" action="./controllers/processa_login.php" method="post">
		<br>
			<h2 class="form-signin-heading">Login</h2>
			
			<label for="email" class="sr-only">Email Institucional</label>
			<input type="email" 	name="email" 	class="form-control"  placeholder="Email" required autofocus>
			
			<label for="senha" class="sr-only">Senha</label>
			<input type="password" 	name="senha" 		class="form-control"  placeholder="Senha" required>
			
			<button class="btn btn-lg btn-primary btn-block" name="login" type="submit">Entrar</button>
	</form>