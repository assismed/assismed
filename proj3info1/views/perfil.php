<head>
  <link href="bootstrap-3.3.6-dist/css/perfil.css" rel="stylesheet">
    <script src="bootstrap-3.3.6-dist/js/perfil.js"></script>
</head>
<section id="container">
<figure class="snip1336">
  <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample87.jpg" alt="sample87" />

  <figcaption>
     <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/profile-sample4.jpg" alt="profile-sample4" class="profile" />
    <h2>Christian Friedrich Samuel Hahnemann<span>Fisioterapeuta</span></h2>
    <p>"Se não há subidas e descidas na sua vida, então você está morto!"</p>
   
    <a href="#" class="info">Ver Perfil</a>
  </figcaption>
</figure>




<div class="">
  <div class="mobile">
    <!-- Checkbox to toggle the menu -->
    <input type="checkbox" id="tm" />
    <!-- The menu -->
    <ul class="sidenav">
      
      <li><a href="#"><i class="fa fa-inbox"></i><b>Mensagens</b></a></li>
      <li><a href="#"><i class="fa fa-pencil"></i><b>Atualizar Cadastro</b></a></li>
      <li><a href="#"><i class="fa fa-cog"></i><b>Excluir Cadastro</b></a></li>
      
    </ul>
    <!-- Content area -->
    <section>
      <!-- Label for #tm checkbox -->
      <label for="tm"><span>menu</span> <i class="fa fa-chevron-right"></i></label>
    </section>
  </div>
</div>
</section>

