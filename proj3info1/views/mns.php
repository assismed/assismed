

  <head>
   <link href="bootstrap-3.3.6-dist/css/menu_toggle.css" rel="stylesheet">
   <script src="bootstrap-3.3.6-dist/js/menu_toggle.js"></script>
  </head>
  <body>
   
    <div class="allholder">
      
 
        
      <div class="feed">  <!--1 Column Feed Item Holder-->
        
        
        <div class="feed-item blog">
          <div class="icon-holder"><div class="icon"></div></div>
          <div class="text-holder col-3-5">
            <div class="feed-title">Blog Item</div>
            <div class="feed-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia natus obcaecati consequuntur quis molestias! Minima impedit ad omnis. Libero quibusdam facere dignissimos ut mollitia unde sunt nobis quia, nam quasi!
            </div>
          </div><!--End of Text Holder-->
          
         
         <div class="post-options-holder">
            <div class= "tools"> 
              <i class="fa fa-ellipsis-v" id="postsettings"></i>
            </div><!--End Tools-->
          </div><!--End Post Options Holder -->
          
        </div><!--End of Feed Item-->
        
    
        
        
        <div class="feed-item blog">
          <div class="icon-holder"><div class="icon"></div></div>
          <div class="text-holder col-3-5">
            <div class="feed-title">Blog Item</div>
            <div class="feed-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia natus obcaecati consequuntur quis molestias! Minima impedit ad omnis. Libero quibusdam facere dignissimos ut mollitia unde sunt nobis quia, nam quasi!
            </div>
          </div><!--End of Text Holder-->
          <div class="post-options-holder">
            <div class= "tools"> 
              <i class="fa fa-ellipsis-v" id="postsettings"></i>
            </div><!--End Tools-->
          </div><!--End Post Options Holder -->
        </div><!--End of Feed Item-->
        
        <div class="feed-item">
          <div class="icon-holder"><div class="icon"></div></div>
          <div class="text-holder col-3-5">
            <div class="feed-title">Other Item</div>
            <div class="feed-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia natus obcaecati consequuntur quis molestias! Minima impedit ad omnis. Libero quibusdam facere dignissimos ut mollitia unde sunt nobis quia, nam quasi!
            </div>
          </div><!--end of text-holder-->
          <div class="post-options-holder">
            <div class= "tools"> 
              <i class="fa fa-ellipsis-v" id="postsettings"></i>
            </div><!--End Tools-->
          </div><!--End Post Options Holder -->
        </div><!--end of feed item-->
        
      
          
        <div class="feed-item blog">
          <div class="icon-holder"><div class="icon"></div></div>
          <div class="text-holder col-3-5">
            <div class="feed-title">Blog Item</div>
            <div class="feed-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia natus obcaecati consequuntur quis molestias! Minima impedit ad omnis. Libero quibusdam facere dignissimos ut mollitia unde sunt nobis quia, nam quasi!
            </div>
          </div><!--End of Text Holder-->
          <div class="post-options-holder">
            <div class= "tools"> 
              <i class="fa fa-ellipsis-v" id="postsettings"></i>
            </div><!--End Tools-->
          </div><!--End Post Options Holder -->
        </div><!--End of Feed Item-->
        
        <div class="feed-item">
          <div class="icon-holder"><div class="icon"></div></div>
          <div class="text-holder col-3-5">
            <div class="feed-title">Other Item</div>
            <div class="feed-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia natus obcaecati consequuntur quis molestias! Minima impedit ad omnis. Libero quibusdam facere dignissimos ut mollitia unde sunt nobis quia, nam quasi!
            </div>
          </div><!--end of text-holder-->
          <div class="post-options-holder">
            <div class= "tools"> 
              <i class="fa fa-ellipsis-v" id="postsettings"></i>
            </div><!--End Tools-->
          </div><!--End Post Options Holder -->
        </div><!--end of feed item-->
        
      
        
        <div class="feed-item">
          <div class="icon-holder"><div class="icon"></div></div>
          <div class="text-holder col-3-5">
            <div class="feed-title">Other Item</div>
            <div class="feed-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia natus obcaecati consequuntur quis molestias! Minima impedit ad omnis. Libero quibusdam facere dignissimos ut mollitia unde sunt nobis quia, nam quasi!
            </div>
          </div><!--end of text-holder-->
          <div class="post-options-holder">
            <div class= "tools"> 
              <i class="fa fa-ellipsis-v" id="postsettings"></i>
            </div><!--End Tools-->
          </div><!--End Post Options Holder -->
        </div><!--end of feed item-->
              
    </div><!--end of Feed-->
    </div><!-- End of Content-->   
    </div><!--end of allholder-->
    
   
    
  
    
  
  </body>
