<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="./img/logo4.png">



    <title>AMEP- Leitor</title>

    <!-- Custom CSS -->
    <link href="bootstrap-3.3.6-dist/css/stylish-portfolio.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">


    <link href="bootstrap-3.3.6-dist/css/msn2_escrever.css" rel="stylesheet">
   
    <link href="bootstrap-3.3.6-dist/css/mensagem.css" rel="stylesheet">

    <link href="bootstrap-3.3.6-dist/css/bootstrap.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css/logo_menu.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css/stylish-portfolio.css" rel="stylesheet">
    
  
    <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="bootstrap-3.3.6-dist/css/signin.css" rel="stylesheet">
    <script src="bootstrap-3.3.6-dist/js/ie-emulation-modes-warning.js"></script>
    <script src="bootstrap-3.3.6-dist/js/ie10-viewport-bug-workaround.js"></script>
    <link href="bootstrap-3.3.6-dist/css/starter-template.css" rel="stylesheet">
    <script src="bootstrap-3.3.6-dist/js/jquery.min.js"></script>
    <script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>

    <script src="bootstrap-3.3.6-dist/js/jquery.js"></script>
    <link href="bootstrap-3.3.6-dist/css/menu_leitor.css" rel="stylesheet">
    <link href="estilo.css" rel="stylesheet">




  </head>

  <body>

 <div class="navbar navbar-default navbar-fixed-top">
        <div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                 <a class="nav navbar-nav navbar-right" href="#">
                    <img src="./img/logo3.png" alt="" id="logo">
                </a>
            </div>
            <div class="navbar-collapse collapse">
                

 <!-- Navigation -->
    <a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
    <nav id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
            <li class="sidebar-brand">
                <a href="#" onclick=$("#menu-close").click();>Minha Conta</a>
            </li>
            <li>
                <a href="?pgs=Cadastro_leitor_atualiza" onclick=$("#menu-close").click();>Atualizar Dados</a>
            </li>
            <li>
                <li><a data-toggle="modal" data-target="#myModal" href="views/tipos_cadastro.php" onclick=$("#menu-close").click();>Excluir Cadastro</a></li>
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                  </div>
                </div>
              </div>
            </li>
            <li>
                <a href="#services" onclick=$("#menu-close").click();>Sair</a>
            </li>
        </ul>
    </nav>

                    
                
            </div>

        </div>
    </div>


<br><br>
 <nav >
      
       <div>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php" class="a">INICIAL</a></li>
            <li><a href="index2_leitor_logado.php" class="a">INICIAL leitor</a></li> 
            <li><a href="index3_pro_logado.php" class="a">INICIAL prof</a></li> 
            <li><a href="?pgs=mns" class="a">HISTÓRICO DE MENSAGENS</a></li>
            
          </ul>
          <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right">
            <div class="form-group">
              <input type="text" placeholder="digite aqui..." class="form-control">
            </div>
            <button type="submit" class="btn btn-success" id= "button">PESQUISAR</button>
          </form>
        </div><!--/.navbar-collapse -->
        </div><!--/.nav-collapse -->

      </div>

    </nav>






    <div class="container">



      
    <?php 
      include_once 'functions.php'; 
      ins_dados(filter_input(INPUT_GET, 'pgs'));
    ?>



             
    </div> <!-- /container -->
    <footer>
        <section>
<a href="?pgs=leitor_no_artigo">Pesquisa por artigo</a><br>
<a href="?pgs=leitor_no_pro">Pesquisa por Profissional</a><br>
<a href="?pgs=leitor_no_perfil_pro">Pesquisa por Perfil do Profissional</a>
</section>
    </footer>

<!-- Custom Theme JavaScript -->
    <script>
    // Closes the sidebar menu
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    // Opens the sidebar menu
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    // Scrolls to the selected menu item on the page
    $(function() {
        $('a[href*=#]:not([href=#],[data-toggle],[data-target],[data-slide])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
    //#to-top button appears after scrolling
    var fixed = false;
    $(document).scroll(function() {
        if ($(this).scrollTop() > 250) {
            if (!fixed) {
                fixed = true;
                // $('#to-top').css({position:'fixed', display:'block'});
                $('#to-top').show("slow", function() {
                    $('#to-top').css({
                        position: 'fixed',
                        display: 'block'
                    });
                });
            }
        } else {
            if (fixed) {
                fixed = false;
                $('#to-top').hide("slow", function() {
                    $('#to-top').css({
                        display: 'none'
                    });
                });
            }
        }
    });
    // Disable Google Maps scrolling
    // See http://stackoverflow.com/a/25904582/1607849
    // Disable scroll zooming and bind back the click event
    var onMapMouseleaveHandler = function(event) {
        var that = $(this);
        that.on('click', onMapClickHandler);
        that.off('mouseleave', onMapMouseleaveHandler);
        that.find('iframe').css("pointer-events", "none");
    }
    var onMapClickHandler = function(event) {
            var that = $(this);
            // Disable the click handler until the user leaves the map area
            that.off('click', onMapClickHandler);
            // Enable scrolling zoom
            that.find('iframe').css("pointer-events", "auto");
            // Handle the mouse leave event
            that.on('mouseleave', onMapMouseleaveHandler);
        }
        // Enable map zooming with mouse scroll when the user clicks the map
    $('.map').on('click', onMapClickHandler);
    </script>

  </body>
</html>
