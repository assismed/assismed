<?php

function ins_dados($pgs) {
	
	if(isset($pgs)){

		
		$arquivo = "views/".$pgs.".php";
		
		if(file_exists($arquivo)){
			include_once $arquivo;
		} else {
			echo "<h4>a pagina $pgs nao existe</h4>";
		}


	} else {
		include_once "views/inicial.php";
	}
}

function valida_dados ($dado){
    $dado = trim($dado);
    $dado = stripcslashes($dado);
    $dado = htmlspecialchars($dado);
    return $dado;
}


